
package ws.core;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="addValueReturn" type="{http://www.w3.org/2001/XMLSchema}float"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "addValueReturn"
})
@XmlRootElement(name = "addValueResponse")
public class AddValueResponse {

    protected float addValueReturn;

    /**
     * Gets the value of the addValueReturn property.
     * 
     */
    public float getAddValueReturn() {
        return addValueReturn;
    }

    /**
     * Sets the value of the addValueReturn property.
     * 
     */
    public void setAddValueReturn(float value) {
        this.addValueReturn = value;
    }

}
