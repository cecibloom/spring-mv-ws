package com.spring.mvc.controller;

import java.text.DateFormat;
import java.util.Date;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.spring.mvc.model.User;
import com.spring.mvc.service.UserService;

import ws.core.HelloWorld;
import ws.core.HelloWorldService;


@Controller
public class HomeController {

	@Autowired
	private UserService userService;

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String home(Locale locale, Model model) {
		System.out.println("Home Page Requested, locale = " + locale);
		Date date = new Date();
		DateFormat dateFormat = DateFormat.getDateTimeInstance(DateFormat.LONG, DateFormat.LONG, locale);

		String formattedDate = dateFormat.format(date);

		model.addAttribute("serverTime", formattedDate);

		return "home";
	}

	@RequestMapping(value = "/user", method = RequestMethod.POST)
	public String user(@Validated User user, Model model) {
		System.out.println("User Page Requested");
		String username = user.getUsername();
		System.out.println("username: " + username);
		model.addAttribute("username", user.getUsername());
		model.addAttribute("name", userService.getUserDetails(username).getFirstName() + userService.getUserDetails(username).getLastName());
		model.addAttribute("phone", userService.getUserDetails(username).getPhone());
		model.addAttribute("email", userService.getUserDetails(username).getEmail());
		
		try {
			HelloWorldService service = new HelloWorldService();
			HelloWorld hello = service.getHelloWorld();
			
			model.addAttribute("ws", hello.addValue(500));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return "user";
	}
}
