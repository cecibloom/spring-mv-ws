/**
 * 
 */
package com.spring.mvc.service;

import com.spring.mvc.model.User;

public interface UserService {

	User getUserDetails(String username);

}
