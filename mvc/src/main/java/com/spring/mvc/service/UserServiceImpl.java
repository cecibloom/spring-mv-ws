/**
 * 
 */
package com.spring.mvc.service;

import org.springframework.stereotype.Service;

import com.spring.mvc.model.User;

@Service
public class UserServiceImpl implements UserService {

	public User getUserDetails(String username) {
		User ud1 = new User();
		System.out.println(username);
		if (username.equalsIgnoreCase("cecibloom")) {
			ud1.setEmail("cecilia.lopgo@gmail.com");
			ud1.setFirstName("Cecilia");
			ud1.setLastName("Lopez");
			ud1.setPhone("+123456789");
		} else if (username.equalsIgnoreCase("alessia")) {
			ud1.setEmail("alessia@gmail.com");
			ud1.setFirstName("Alessia");
			ud1.setLastName("Cernelli");
			ud1.setPhone("+9999999");
		} else if (username.equalsIgnoreCase("piergiorgio")) {
			ud1.setEmail("piergiorgio@gmail.com");
			ud1.setFirstName("Piergiorgio");
			ud1.setLastName("Spaziani");
			ud1.setPhone("+555555555");
		} else {
			ud1.setEmail("johndoe@gmail.com");
			ud1.setFirstName("John");
			ud1.setLastName("Doe");
			ud1.setPhone("+00000");
		}
		
		return ud1;
	}

}
